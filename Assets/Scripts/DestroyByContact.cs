﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    public GameObject AsteroidExplosion, PlayerExplosion;
    private GameController gameController;
    public int scoreValue;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        if(gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }

        if(gameController == null)
        {
            Debug.Log("Unable to find 'GameController' ");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Boundary"))
        {
            return;
        }
        Instantiate(AsteroidExplosion, gameObject.transform.position , gameObject.transform.rotation);
        gameController.AddScore(scoreValue);
        if (other.gameObject.CompareTag("Player"))
        {
            Instantiate(PlayerExplosion, other.gameObject.transform.position  , other.gameObject.transform.rotation);
        }
        Destroy(other.gameObject);
        Destroy(gameObject);
        
    }
}
